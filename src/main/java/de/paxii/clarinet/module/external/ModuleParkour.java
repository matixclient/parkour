package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerUpdateWalkingEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

/**
 * Created by Lars on 05.07.17.
 *
 * @author Alexander01998
 * https://github.com/Wurst-Imperium/Wurst-MC-1.12/blob/master/shared-src/net/wurstclient/features/mods/movement/ParkourMod.java
 */
public class ModuleParkour extends Module {

  public ModuleParkour() {
    super("Parkour", ModuleCategory.MOVEMENT);

    this.setVersion("1.0");
    this.setBuildVersion(18200);
    this.setDescription("Automatically jumps when you reach the edge of a block");
    this.setRegistered(true);
  }

  @EventHandler
  public void onUpdateWalking(PlayerUpdateWalkingEvent playerUpdateWalkingEvent) {
    boolean shouldJump = Wrapper.getPlayer().onGround
        && !Wrapper.getPlayer().isSneaking()
        && !Wrapper.getGameSettings().keyBindSneak.isKeyDown()
        && !Wrapper.getGameSettings().keyBindJump.isKeyDown();

    boolean onEdge = Wrapper.getWorld().getCollisionBoxes(
        Wrapper.getPlayer(),
        Wrapper.getPlayer().getEntityBoundingBox().offset(0, -0.5, 0).expand(-0.001, 0, -0.001)
    ).isEmpty();

    if (shouldJump && onEdge) {
      Wrapper.getPlayer().jump();
    }
  }

}
